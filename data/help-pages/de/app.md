# Einführung

In Android-Apps sind sind häufig sogenannte SDKs (Software Development Kits) integriert. 

SDKs sind Software-Module, die von Programmierer'innen einfach in die eigene App integriert werden können und dann bestimmte Funktionen erledigen. 

Mit SDKs wird oft auch getrackt. Anbieter wollen erfahren, wie die App genutzt wird. Oder sie tracken für Werbezwecke. Fast immer wird dabei eine eindeutige ID des Smartphones mitgesendet, häufig ist es die Android Adervtiser ID (AAID). 

**Wer eine App nutzt, hat ein Recht darauf, dass die App fragt, bevor sie trackt!**

Wie man herausfindet, ob eine App unerlaubt trackt, zeigt dieses Tutorial. 

# Gibt es eine Einwilligungsfrage?
Bevor eine App für Marktforschung oder Werbung tracken darf, muss sie laut Gesetz vorher eine Einwilligung einholen. Das machen sehr viele Apps leider nicht. 

So sieht eine gültige Einwilligungsfrage aus: 

<img src="images/app/Screenshot_congstar.png" width="200"/>

Naja, eigentlich darf die Ablehnung nicht so versteckt sein wie hier, aber das übergehen wir mal. Aber es muss auf jeden Fall eine Option zum Ablehnen geben, sonst ist die Einwilligung nicht freiwillig und daher ungültig. Im Normalfall wird nur einmal beim ersten Start der App gefragt. Daher muss man die App vor der Beschwerde neu installieren. 



# Prüfung auf SDKs

## 1. Welche SDKs sind enthalten? 


Mit Hilfe von zwei Websites kann man sehr einfach herausfinden, ob eine App überhaupt solche SDKs enthält. Beide Seiten prüfen laufend den Programmiercode von Apps auf Tracking-SDKs. 
### Mobilsicher
Unter [appcheck.mobilsicher.de](https://appcheck.mobilsicher.de/) findet man zu fast allen Apps einen *Schnelltest*. Man kann nach dem **Namen** der Apps suchen. Dann werden die Softwaremodule aufgelistet. 
<img src="images/app/mobilsicher_phase6.png" width="500"/>
Hier sieht man unter anderem das SDK *Adjust*, welches nur mit Einwilligung zulässig ist.

### Exodus Privacy
Unter [reports.exodus-privacy.eu.org](https://reports.exodus-privacy.eu.org/en/reports/) kann man nach dem **Namen** und auch der **Playstore-URL** einer App suchen. Auch hier werden die gefundenen SDKs von sehr vielen Apps angezeigt. Auch hier sieht man das SDK von *Adjust*. 
<img src="images/app/exodus_phase6.png" width="500"/>


## 2. Welche SDKs sind aktiv? 
Jetzt wissen wir, welche SDKs in der App vorhanden sind. Aber wir wissen nicht, ob sie aktiv sind. Das ist nicht immer der Fall. Wir müssen also den Datenverkehr der App prüfen. 

### Verbindungen prüfen mit PCAPdroid
Die App *PCAPdroid* ([Website](https://emanuele-f.github.io/PCAPdroid/) | [F-Droid](https://f-droid.org/packages/com.emanuelef.remote_capture/) | [Play Store](https://play.google.com/store/apps/details?id=com.emanuelef.remote_capture)) kann den Datenverkehr des Smartphones mitschneiden und anzeigen. Dafür wird intern eine VPN-Verbindung angelegt und durch die App geleitet. So sieht man für jede App den aktuellen Datenverkehr. 

1. Installiere die App, die du prüfen willst. Starte sie noch nicht. 
2. Starte PCAPdroid und gehe auf den Play-Button rechts oben (▶️). Nun zeichnet die App auf. 
3. Starte die App, die du prüfen willst und bediene sie eine Weile. 
4. Wechsle zurück auf PCAPdroid und stoppe die Aufzeichnung (■).
5. Filtere die Anzeige, so dass NUR die zu untersuchende App angezeigt wird. 

<img src="images/app/PCAPdroid_phase6.png" width="200"/>

In dieser Liste sieht man, dass die App *Phase6* Verbindungen zu app.adjust.com aufbaut. 

Jetzt haben wir belegt, dass das SDK Daten versendet und aktiv ist. Für ein Anschreiben reicht das in jedem Fall. Auch eine Beschwerde kann man damit einreichen, da die Behörde die App im Zweifelsfall auch selbst prüfen kann. Ein Screenshot von PCAPdroid belegt die Verbindung.

# Für Fortgeschrittene: Entschlüsseln des Datenverkehrs

Schneller und schlagkräftiger ist die Beschwerde aber, wenn man wirklich die Übertragung von pseudonymen IDs belegen kann. Dazu müssen wir den Datenverkehr entschlüsseln.

## Setup

Auf dem Computer (Linux)

* `mitmproxy` 
* `python` und `pip`
* `frida` und `objection`  

Auf dem Android-Smartphone

* Root-Zugang mit `adb`
* PCAPdroid 
* `frida-server`



## Installation


Die App PCAPdroid ist ja bereits auf dem Smartphone installiert. Alle weiteren Sachen müssen von Linux aus erledigt werden. 

### Mitmproxy 

Wir nutzen im folgenden die [Mitmproxy-Variante von PCAPdroid](https://github.com/emanuele-f/mitmproxy), weil der Datenverkehr so ohne umständliche iptables-Konfiguration auf den Linuxrechner umgeleitet wird. Ein weitere Vorteil der Kombi ist, dass man zeitgleich alle Anfragen in PCAPdroid sieht, auch die, die von mitmproxy nicht entschlüsselt werden konnten. Wer möchte kann [Mitmproxy](https://mitmproxy.org/) aber auch als eigenständiges Tool ohne PCAPdroid verwenden ([Tutorial](https://blog.heckel.io/2013/07/01/how-to-use-mitmproxy-to-read-and-modify-https-traffic-of-your-phone/)). 

```bash
$ git clone https://github.com/emanuele-f/mitmproxy
$ cd mitmproxy
$ sudo python3 setup.py install
```

### Zertifikat installieren
Nach der Installation wurde eine Zertifikatsdatei *mitmproxy-ca-cert.cer* unter `~/.mitmproxy/`erstellt. Diese müssen wir auf bestimmte Weise umbennenen und aufs Smartphone kopieren. Dafür erst den Hash berechnen:

```bash
$ openssl x509 -inform PEM -subject_hash -in mitmproxy-ca-cert.cer | head -1

c8450d0d
```

An den Hash wird `.0` angehängt und bildet so den neuen Dateinamen. 

Wenn der Hash z.B. *c8450d0d* ist, dann ist unser Dateiname *c8450d0d.0*:

```bash
$ cp mitmproxy-ca-cert.cer c8450d0d.0
```

Jetzt wollen wir mit adb auf das Smartphone zugreifen. Falls es noch nicht auf Linux installiert ist:

```bash
$ sudo apt-get install adb
```

Den Zugriff müssen wir auf dem Android-Gerät erlauben, dazu brauchen wir die Entwicklereinstellungen, die man einmalig mit sieben mal Tippen auf die Build-Nummer aktiviert (unter *Einstellungen/System/Über das Telefon*). 

In den Entwicklereinstellungen *Root-Zugriff über ADB* aktivieren. Dann im gleichen Menu *Android-Debugging über ADB* anschalten. 

Das Smartphone anstecken. Nun sollte ein Gerät erscheinen:

```bash
$ adb devices   
List of devices attached
* daemon not running; starting now at tcp:5037
* daemon started successfully
4767b257	device
```
Wir aktivieren den Root-Zugang und starten die Shell im Smartphone:

```bash
$ adb root
$ adb shell
```

In der Shell des Smartphones müssen wir nur kurz das System mit Schreibrechten einhängen. Dann verlassen wir die Smartphone-Shell wieder. 

```bash
android:/$ mount -o rw,remount /system
android:/$ exit
```

Zurück auf Linux können wir das Zertifikat ins Smartphone auf Systemebene kopieren. 

```bash
$ adb push c8450d0d.0 /system/etc/security/cacerts/
```
Wieder in die Smartphone-Shell wechseln und die Dateirechte anpassen:

```bash
$ adb shell 
android:/$ chmod 664 /system/etc/security/cacerts/c8450d0d.0
```

Anschließend sollte man das Smartphone neu starten und unter *Einstellungen/Sicherheit & Datenschutz/Verschlüsselung & Anmeldedaten/Vertrauenswürdige Anmeldedaten* im Reiter *System* zum Zertifikat von *mitmproxy" scrollen und sicherstellen, dass der Schalter aktiviert ist. 

Der Entwickler von PCAPdroid beschreibt noch eine weitere [Methode mit VirtualXposed](https://emanuele-f.github.io/PCAPdroid/tls_decryption), um das Zertifikat-Pinning zu umgehen, falls man über `adb` keinen Root-Zugang erhält. 

### Frida und Objection
Jetzt haben wir ein Zertifikat auf das Smartphone geschleust, um den Datenverkehr entschlüsseln zu können. Aber gegen diese Methode gibt es eine Sicherheitsmethode in fast allen Apps: Das sogenannte Zertifikat-Pinning verhindert in der App die Verwendung von selbsterstellten Zertifikaten. Um das zu umgehen kann man das App-Hacking-Tool [Frida](https://frida.re/) mit dem Modul [Objection](https://github.com/sensepost/objection/wiki/Installation) einsetzen. Das sollte in 90% der Fälle funktionieren. 

Wir installieren Frida unter Linux:

```bash
$ pip install frida-tools
```

Nun brauchen wir die Gegenstelle von Frida für Android: *[frida-server](https://frida.re/docs/android/)*: Je nach Prozessor-Architektur des Smartphones die richtige Version wählen und auf der [Release-Seite von Frida](https://github.com/frida/frida/releases) manuell herunterladen. Findet man die Architektur nicht über Suchmaschinen heraus, einfach alle aufs Smartphone laden und ausprobieren. 

```bash
$ unxz frida-server-14.2.14-android-arm64.xz 
$ adb root
$ adb push frida-server-14.2.14-android-arm64 /data/local/tmp
$ adb shell
android:/$ cd /data/local/tmp
android:/$ chmod 755 frida-server-14.2.14-android-arm64
```

Und gleich mal testen:

```bash
android:/$ ./frida-server-14.2.14-android-arm64
```
Wenn keine Fehlermeldung kommt und die Shell durch die Ausführung belegt bleibt, läuft frida-server. 

Nun installieren wir unter Linux noch das Frida-Modul [Objection](https://github.com/sensepost/objection/wiki/Installation):

```bash
$ pip3 install -U objection
```

### Prüfung des Datenverkehrs
Wenn alle Tools installiert sind, können wir die Entschlüsselung und Prüfung der übertragenen Daten beginnen. 

Wir beginnen auf dem Smartphone: Die App sollte für die Prüfung neu installiert werden und dann noch nicht gestartet worden sein. Alternativ kann man auch den kompletten Speicher der App löschen (*Einstellungen/Apps/[gewünschte App]/Speicher/Daten löschen*. Achtung: Alle Einstellungen, Inhalte und Login-Daten sind dann gelöscht.  

Das Smartphone dann über USB mit dem Computer verbinden. 

PCAPdroid starten und *Enable TLS Decryption* wählen. Unter *mitmproxy IP Address* die lokale IP des Linuxrechners angeben. 

Dann von Linux aus frida-server starten:

```bash
$ adb root
$ adb shell 
android:/$ cd /data/local/tmp
android:/$ ./frida-server-14.2.14-android-arm64
```

Eine neue Shell auf Linux öffnen und mitmproxy starten.

```bash
$ mitmproxy --mode tunnel --listen-port 8080
```

Wenn wir nun eine App starten, wird in diesem Fenster nur die Fehlermeldung kommen: "Client Handshake failed". Die Aufrufe werden also noch nicht entschlüsselt.  

In einer dritten Shell nutzen wir nun Objection. Zuerst sollte man das Setup mit einer vertrauten App testen. Das ist wichtig um herauszufinden, ob das Setup funktioniert oder ein spezielles Problem mit einer App vorliegt. Ich nutze dafür immer F-Droid. 

Wir starten in diesem Kommando von Linux aus die App und unterdrücken gleichzeitig das Zertifikat-Pinning. Die volle Bezeichnung der App findet man übrigens leicht über die App-Info in den Einstellungen. 

```bash
$ objection --gadget "org.fdroid.fdroid" explore --startup-command 'android sslpinning disable'
```

Mit ein wenig Scrollen in F-Droid sollten jetzt in mitmproxy die Requests zu den Thumbnails sichtbar werden. 

<img src="images/app/fdroid_mitmproxy.png" width="500"/>

Wenn das Setup steht, können wir uns endlich eine App mit Trackern anschauen. Hier z.B. der Vokabeltrainer Phase6:

```bash
$ objection --gadget "de.phase6.freeversion.beta" explore --startup-command 'android sslpinning disable'
```

Direkt nach dem Start macht die App vier Verbindungen auf. Das sind die Tracker von Adjust und Amplitude. Wir sehen am unteren Rand wieder die Warnung "Client Handshake failed". Es gibt also Verbindungen, die wir nicht entschlüsseln können. In diesem Fall können wir in PCAPdroid durch die Requests gehen, die zeitgleich aufgenommen wurden und sehen, dass es die Verbindungen zu Google Firebase sind. 

Um die vier Requests genauer zu betrachten, sind folgende Tastenkombination in mitmproxy hilfreich: 

Taste  	    |  Funktion
----------  |  ----------
↑ ↓ |  In den Requests nach oben/unten bewegen
Enter |  Request genauer anschauen
← → | In den Tabs eines Requests bewegen (Request, Response, Detail)
q  | Request verlassen und zur Liste zurückkehren
e | Mit >> markierten Request der Liste speichern 

Im Detail eines Requests (Enter) sehen wir nun die übertragenen IDs:
<img src="images/app/phase6_adjust_request.png" width="700"/>


Einen Screenshot wie diesen kann man als Beleg an seine Beschwerde anhängen. Alternativ den Request einzeln speichern. 

### Mitmweb

Alternativ zu `mitmproxy` kann man den Verkehr auch mit `mitmweb` betrachten. Durch Anzeige und Steuerung innerhalb eines Browser ist das Tool viel leichter zu bedienen: 

```bash
$ mitmweb --mode tunnel --listen-port 8080
```
<img src="images/app/phase6_mitmweb.png" width="700"/>


---

💡 **Tipp:**  In dieser Ansicht lassen sich auch alle Requests einer Sitzung speichern (im Menü: *mitmproxy/Save ...*). Das ist perfekt für die spätere Auswertung und als Beleg für die Beschwerde.

---
