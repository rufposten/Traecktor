# Vorbereitung

Diese Anleitung ist für Firefox. Die Analyse von Webseiten-Tracking funktioniert aber ähnlich in Safari, Chrome und Internet Explorer. 

Wer die notwendigen Einstellungen nicht in seinem Standardbrowser ändern möchte, [legt dafür ein weiteres Profil im Browser an](https://support.mozilla.org/de/kb/firefox-profile-erstellen-und-loeschen) (unter [about:profiles](about:profiles)). 


## Trackingblocker deaktivieren
Um zu erkennen, ob eine Webseite Tracker integriert hat, müssen wir zunächst Ad- und Trackingblocker deaktivieren: Diese verfälschen das Ergebnis in beide Richtungen: Sie können Tracker blocken und anderseits ein funktionierendes Einwilligungsmanagement stören. 

1. In [Einstellungen/Datenschutz & Sicherheit](about:preferences#privacy) den Schutz vor Aktivitätenverfolgung auf *benutzerdefiniert* stellen und alle Punkte deaktivieren.  
<img src="images/web/firefox_benutzerdefiniert.png" width="500"/> 
1. Einmalig *Cookies und Website-Daten/Daten entfernen*
1. Dort dauerhaft einen Haken setzen
<img src="images/web/firefox_daten_entfernen.png" width="500"/> 


## Netzwerkanalyse richtig einstellen

Die *Netzwerkanalyse* von Firefox öffnen (Strg-Shift-E). 

<ol>
<li>Mit der rechten Taste auf die Kopfspalte der Tabelle und dann die Spalten auswählen, die noch nicht angezeigt werden: Wir brauchen vor allem Host, Datei und Cookies.<br /><br /></li>
<li>Kreuz setzen bei *Cache deaktivieren*
	<img src="images/web/firefox_cache_columns.png" width="500"/><br /><br />
	</li>
<li>Dann zum rechten Optionsrad und Kreuz setzen bei *Logs nicht leeren* 
<img src="images/web/firefox_logsnichtleeren.png" width="500"/><br /><br /> 
</li>
</ol>




# Analyse

## Tracker finden

Für die Analyse in einem leeren Tab die Netzwerkanalyse mit Strg-Shift-E öffnen. Dann erst die URL eingeben. Nun sieht man die Request, die beim Seitenaufruf rausgehen. Hier sind auch alle Trackingeinbettungen sichtbar. 

In der Tracktor.it!-Liste haben wir die typischen URLs der wichtigsten Tracker aufgelistet. Man kann sie mit dem Kopiersymbol &nbsp; <img src="images/web/copy_symbol.png" style="height:1em; display:inline" /> &nbsp; schnell kopieren und dann ins das Filterfeld kopieren (*Adressen durchsuchen*). 



Erscheint nach dem Filtern eine passende Anfrage, ist der Tracker in der Seite. 

## Evtl. Cookies und Parameter prüfen
Bei manchen Trackern muss man prüfen, ob bestimmte Cookies oder Parameter übertragen wurden, die personenbeziehbar sind. Nur dann ist eine Beschwerde möglich. Darauf weisen wir immer in der Trackerbeschreibung hin. 

Mit einem Klick auf eine Anfrage sehen wir zunächst die Kopfzeilen. Nur bei wenigen Trackern müssen wir hier nachsehen, wie hier beim Parameter *_kuid* von Salesforce. 

<img src="images/web/netzwerkanalyse_parameter.png" width="800"/> 



Häufiger interessiert uns der Tab *Cookies*. Hier sehen wir beispielsweise das IDE-Cookie, das für Google Ads eine Profilbildung belegt: 
<img src="images/web/netzwerkanalyse_cookies.png" width="800"/> 

💡 **Tipp:** Cookies sollte man immer in der Netzwerkanalyse suchen. Im Tab *Web-Speicher* sehen wir auch Cookies - aber nur die aktuell vorhandenen. Während eine Seite aufgebaut wird, werden Cookies aber in Milisekunden gelesen, an Partner übertragen und wieder gelöscht und das sieht man alles nur in der Netzwerkanalyse. Rechtlich ist es übrigens egal, ob eine Cookie von einer Seite gesetzt wird (*Antwort-Cookies*) oder nur ausgelesen wird (*Anfrage-Cookies*). Entscheidend ist der "Zugriff". 

# Cookie-Walls
Will man gegen Tracking auf Websites vorgehen, muss man die Spielregeln für Einwilligungslösungen kennen.

## Fakt 1: Cookies brauchen nicht grundsätzlich eine Erlaubnis. 
Nur bestimmte Cookies für personalisierte Werbung oder pseudonyme Sitzungsstatistiken (z.b. Google Analytics) brauchen eine Einwilligung. Aber um diese Details kümmern wir uns, Hinweise dazu gibt es in der Trackerliste.  



## Fakt 2: Was man ignorieren kann, darf man ignorieren
Vorausgewählte Kästchen oder Cookie-Einblendungen am Seitenrand (Cookie-Banner) dürfen von Besuchern ignoriert werden: Weitersurfen oder keine Einstellungen zu ändern gilt nicht als Einwilligung. 
<img src="images/web/globetrotter_cookiebanner.png" width="600"/> 



## Fakt 3: Eine Seite muss nicht kostenlos sein
Pur-Abos wie bei Zeit, Spiegel oder DerStandard dürfen die Alternative zur Einwilligung sein. Der Preis muss aber realistisch sein (typischerweise 5€ pro Monat). 
<img src="images/web/wall_standard.png" width="400"/> 



## Fakt 4: Dark Patterns sind nicht erlaubt
Eine Einwilligung muss eine freie Willensäußerung sein und keine psychologisch erzwungene Zufallswahl. Dark Patterns überschreiten die Grenze der inhaltlichen Überzeugung. **ABER:** Da wir noch keine Erfahrung mit solchen Verfahren sammeln konnten, kann *Tracktor.it!* aber aktuell noch nicht gegen Dark Patterns vorgehen. 
<img src="images/web/voelkner_darkpattern.png" width="400"/> 



### Der Weg durch die Einwilligungswand
Für die Prüfung muss man also nur einen Weg durch eine sperrende Cookie-Wall finden, ohne dabei mit einem Schalter oder Button einzuwilligen. Der besonders verwirrende IAB-Standarbanner kann im folgenden Beispiel also unbesehen über "Auswahl speichern" umgangen werden. Sollte ein Schalter aktiviert sein, gilt das nicht als gültige Einwilligung und es ist genauso, wie wenn der Tracker ohne Cookie-Wall eingebunden wäre. 
<img src="images/web/stern_iab.png" width="600"/> 







 

