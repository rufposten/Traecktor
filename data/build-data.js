const fs = require("fs");
const path = require("path");
const marked = require("marked");

const { copyFolderRecursiveSync } = require("./utils");

const schemaToTypescript = require('json-schema-to-typescript');

const savedir = path.join(__dirname, "../generator/src/data/");
if (!fs.existsSync(savedir))
    fs.mkdirSync(savedir);
const savedirphp = path.join(__dirname, "../web/data/");
if (!fs.existsSync(savedirphp))
    fs.mkdirSync(savedirphp);

// This ain't exactly pretty but globally remember the file name so we don't have to manually pass it to `fail()`.
let f = undefined;

const fail = (...args) => {
    if (f) console.error(/* bold, bg red */ `\x1b[1m\x1b[41mError in ${f}:\x1b[0m` /* reset */);
    console.error(...args);
    process.exit(1);
};


// *********************************
// create single json and typescript interfaces

function createImportFile (fullpath, ext = "ts", saveat = savedir) {

    let content = [];

    fs.readdirSync(path.join(__dirname, fullpath)).forEach(file => {

        f = fullpath + "/" + file;

        try {
            const data = JSON.parse(fs.readFileSync(path.join(__dirname, fullpath, file)).toString());
            content.push(data);
        } catch (error) {
            fail(error);
        }


    })

    fs.writeFileSync(path.join(saveat, `${fullpath.split("/").join("-")}.${ext}`), (ext === "ts") ? `export default ${JSON.stringify(content)}; ` : JSON.stringify(content));

}

if (!fs.existsSync(savedir)) {
    fs.mkdirSync(savedir);
}


function createInterfaces (fullpath, interfaceName, interfaceNameextendsFrom) {

    schemaToTypescript.compileFromFile(path.join(__dirname, `schema-${fullpath.split("/").join("-")}.json`))
    .then(ts => {

        if (interfaceName) {
            ts += `interface ${interfaceName} extends ${interfaceNameextendsFrom}{};
export default ${interfaceName};
`
        }

        fs.writeFileSync(path.join(savedir, `${fullpath.split("/").join("-")}.d.ts`), ts)
    })

}

createImportFile("supervisory-authorities");
createImportFile("supervisory-authorities", "json", savedirphp);
createInterfaces("supervisory-authorities");
createImportFile(path.join("tracker", "webpage"));
createImportFile(path.join("tracker", "webpage"), "json", savedirphp);
createInterfaces(path.join("tracker", "webpage"), "ITracker", "TrackerDatabase");

createImportFile(path.join("tracker", "app"));
createImportFile(path.join("tracker", "app"), "json", savedirphp);
createInterfaces(path.join("tracker", "app"), "ITrackerApps", "TrackerDatabase");



// *********************************
// copy static files

fs.copyFileSync(path.join(__dirname, "supervisory-authorities-recognize/german-zip-code.json"), path.join(savedir, "german-zip-code.json"));



// *********************************
// create templates json file

let templates = {};

fs.readdirSync(path.join(__dirname, "templates")).forEach(languageCode => {

    templates[languageCode] = {};

    fs.readdirSync(path.join(__dirname, "templates", languageCode)).forEach(templateName => {

        const fileContent = fs.readFileSync(path.join(__dirname, "templates", languageCode, templateName)).toString();

        templates[languageCode][templateName.replace(".txt", "")] = fileContent;
    
    })

})

fs.writeFileSync(path.join(savedir, `templates.ts`), 
`export default ${JSON.stringify(templates)};`);



// *********************************
// create localization file

let language = {}
let localization = path.join(__dirname, "localization");

fs.readdirSync(localization).forEach(languageCode => {

    language[languageCode] = {};

    fs.readdirSync(path.join(localization, languageCode)).forEach(localizationFile => {

        f = localizationFile;

        const fileContent = fs.readFileSync(path.join(localization, languageCode, localizationFile)).toString();

        try {
            language[languageCode][localizationFile.replace(".json", "")] = JSON.parse(fileContent);
        } catch (error) {
            fail(error);
        }
    
    })

})

fs.writeFileSync(path.join(savedir, `localization.ts`), `export default ${JSON.stringify(language)}; `);
fs.writeFileSync(path.join(savedirphp, `localization.json`), JSON.stringify(language));



// *********************************
// parse markdown help pages

let helpPagesRoot = path.join(__dirname, "help-pages");

if (!fs.existsSync(path.join(savedirphp, "help-pages"))) {
    fs.mkdirSync(path.join(savedirphp, "help-pages"));
}

fs.readdirSync(helpPagesRoot).forEach(languageCode => {

    const currentDir = path.join(savedirphp, "help-pages", languageCode);

    if (!fs.existsSync(currentDir)) {
        fs.mkdirSync(currentDir);
    }

    copyFolderRecursiveSync(path.join(helpPagesRoot, languageCode, "images"), path.join(currentDir));

    fs.readdirSync(path.join(helpPagesRoot, languageCode)).forEach(helpPageFileName => {

        if (helpPageFileName.indexOf(".json") > -1) {
            fs.copyFileSync(path.join(helpPagesRoot, languageCode, helpPageFileName), path.join(currentDir, helpPageFileName));
            return;
        }

        if (helpPageFileName.indexOf(".md") === -1) {
            return;
        }

        f = helpPageFileName;

        const markdownContent = fs.readFileSync(path.join(helpPagesRoot, languageCode, helpPageFileName)).toString();

        try {

            let htmlCode = marked(markdownContent);

            // FIXME: maybe it is easier to work with css rules
            const replaceWith = {
                "<p": "<p class='my-4 leading-7 text-l' ",
                "<h3 ": '<h3 class="text-2xl line-break mt-10" ',
                "<h2 ": '<h2 class="text-2xl font-medium line-break mt-10" ',
                "<h1 ": '<h1 class="text-primary text-3xl md:text-4xl font-medium mt-10" ',
                "</video>": '</video><div class="h-16"></div>',
                '<img src="images/': '<img src="/data/help-pages/de/images/',
                "<ol>": "<ol class='list-inside list-decimal m-5'>",
                "<a href=\"http": "<a target='_blank' class='link' href=\"http",
                "<code ": "<pre><code class='my-6 language-bash' ",
                "</code>": "</code></pre>",
                "<table>": '<table class="min-w-full divide-y divide-gray-200 my-10">',
                "<tbody>": '<tbody class="bg-white divide-y divide-gray-200">',
                "<td>": '<td class="px-6 py-4 whitespace-nowrap">',
                "<th>": '<th scope="col" class="px-6 py-3 text-left text-l font-medium">'
            }

            for (tagName in replaceWith) {
                htmlCode = htmlCode.replace(new RegExp(`${tagName}`, "g"), replaceWith[tagName]);
            }

            fs.writeFileSync(path.join(currentDir, helpPageFileName.replace(".md", ".html")), htmlCode);

        } catch (error) {
            fail(error);
        }
    
    })

})