Sehr geehrte Damen und Herren,

ich war gerade auf Ihrer Website {trackerUrl} und habe mit Bedauern festgestellt, dass Sie dort folgende[usesingular>n] Tracker einsetzen:

{trackerText}

{bghRuling}

[consented>Ich sehe gerade: Ich bin mir gar nicht sicher, ob ich auf der Seite eine Einwilligung gegeben habe. Also vergessen Sie das mit der Beschwerde, aber vielleicht können Sie [useplural>die][usesingular>den] Tracker trotzdem entfernen.][consentedMinimal>Eine Einwilligung zum Tracking habe ich übrigens nicht gegeben, sondern nur die angebotene Alternative bzw. die voreingestellte Auswahl mit absolut notwendigen Cookies akzeptiert][selectedConsentButtons> ({selectedConsentButtons})][consentedMinimal>.][notConsented>Eine Einwilligung zum Tracking habe ich übrigens nicht gegeben.]

Ich schätze Ihre Website und habe Verständnis für die praktischen Tools, die für Site-Admins mit diesen Trackern einhergehen. Aber für mich als Websitebesucher ist das Tracking problematisch und ärgerlich. Ich weiß, dass viele andere Seiten auch solche Tracker einbinden, aber irgendwo muss man ja anfangen.

Deshalb würde ich mich freuen, wenn Sie das Tracking ohne Einwilligung in den nächsten Wochen entfernen würden. Bitte beachten Sie aber auch, dass ich nicht zögere, nach Ablauf von vier Wochen eine Beschwerde bei der zuständigen Behörde einzureichen, wenn sich bis dahin nichts geändert hat. Ich möchte mein Recht wirklich auch durchsetzen. Herzlichen Dank für Ihr Verständnis!

Mit freundlichen Grüßen,
{addressOfThePersonConcerned}
