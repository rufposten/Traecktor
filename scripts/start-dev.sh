#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR


function build_data () {
    cd ../data/
    watch -n 2 "npm run watch"
}

function start_react_dev () {
    cd ../generator
    npm start
}

function php_dev_server () {
    cd ../web
    xdg-open "http://localhost:9090"
    php -S 127.0.0.1:9090
}

build_data &>build.log & php_dev_server &>php.log & start_react_dev