<?php 

function correct_date ($a) {

    $a = explode ("-", $a);
    $b = explode (" ", $a[2]);

    $c = $b[0] . "-" .  $a[1] . "-"  . $a[0] . " " . $b[1];
    return $c;

}

$db = new PDO("sqlite:" . __DIR__ . "/new.db");

$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

function update_date ($table) {

    global $db;

    $stmt = $db->query("SELECT * FROM $table");
    $webpage = $stmt->fetchAll();

    foreach($webpage as &$var) {

        if (strlen(strtok($var["created_at"], "-")) === 2) {

            $id = $var["id"];
            $correct = correct_date($var["created_at"]);

            $db->exec("UPDATE $table SET created_at = '$correct' WHERE id = '$id'");

        }

    }
}

update_date("webpage");
update_date("app");