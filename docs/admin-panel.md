# Admin-Panel

Sehr einfaches Admin-Panel zum Bearbeiten/Löschen der Statistiken.

Erreichbar unter https://tracktor.it/admin

Die Zugangsdaten müssen in `/web/admin/index.php` gespeichert werden. Dazu einfach das Array ganz am Anfang der Datei anpassen. 

Die Standard-Anmeldeinformationen, die nur aktiv sind, wenn es sich um einen PHP-Entwicklungsserver handelt (!), sind: `admin:admin`.

## Beispiel

Es soll ein weiterer Benutzer `test` angelegt werden.

1. Passwort generieren (muss mind. 12 Zeichen lang sein!): https://tracktor.it/admin/password.php
2. Benutzer anpassen

```php
# VOHER
$users = [
    "admin" => '$2y$10$dFBkuUa0v.QyKQri3h3j5uTPPsLqhqwSiLK0Z80.SOGhYhzCQXzpy'
];

# DANACH
$users = [
    "admin" => '$2y$10$dFBkuUa0v.QyKQri3h3j5uTPPsLqhqwSiLK0Z80.SOGhYhzCQXzpy',
    "test" => '$2y$10$fdDjS9tZ59ttuR7UUT1t..OlMXUVPqcDKsbMoQaC6S4YcKFTc7Oqq'
]
```