<?php

require_once "./core/localization.php";

$included = true;
$html_title = t("supervisory-authority.title");

require "./include/header.php";
require "./core/utils.php";
require "./statistics/services.php";

$supervisory_authorities = get_supervisory_authority();
$supervisory_authorities_usage = get_supervisory_authorities_usage();

$count_not_assigned =  $supervisory_authorities_usage->{""};

?>

<body>

    <?php require_once "./include/navbar.php" ?>

    <div class="max-w-screen-lg mx-auto px-6 lg:px-0">

        <h1 class="text-primary text-3xl md:text-4xl font-medium"><?php echo t("supervisory-authority.title"); ?></h1>
        <p class="my-3 text-lg"> <?php echo t("supervisory-authority.desc"); ?> </p>
        <?php if ($count_not_assigned > 0): ?>
        <p class="text-xs text-red-500">
            Es gibt <?php echo $count_not_assigned; ?> Beschwerden, die keiner Aufsichtsbehörde zugeordnet sind.
        </p>
        <?php endif; ?>
		<div class="h-10"></div>

        <input onkeyup="filterList(this)" class="my-3 rounded border-2 px-4 py-2 w-full sm:w-1/2 focus:border-primary" placeholder="<?php echo t("supervisory-authority.filter-placeholder"); ?>"/>

        <ul class="list" style="min-height: 50vh">
            <li class="notfound hidden border-2 border-gray-100 p-10 my-8">
                <h3 class="text-2xl font-medium line-break"><?php echo t("supervisory-authority.nomatch"); ?></h3>
            </li>

<?php foreach ($supervisory_authorities as $key => $sa): ?>

            <li class="border-2 border-gray-100 p-4 my-4 relative" data-search="<?php echo strtolower($sa["name"]); ?>">

                <h3 class="text-l font-medium mt-0">
                    <?php echo $sa["name"]; ?>                  
                </h3>

                <p class='my-2 text-xs text-gray-500'>
                    <?php 
                    
                    $count = @$supervisory_authorities_usage->{$sa["slug"]};
                    if ($count > 0) {
                        echo "An diese Behörde wurden bereits <a class='text-primary cursor-pointer' href='/statistics.php?supervisory_authority=".$sa["slug"]."'>" . $count . " Beschwerden</a> geschickt.";
                    } else {
                        echo "Bisher noch keine Beschwerden bekommen.";
                    }
                    ?>
                </p>


            </li>

<?php endforeach; ?>
        </ul>

    </div>

    <script src="assets/js/search.js"> </script>

    <?php require_once "./include/footer.php" ?>

</body>
</html>
