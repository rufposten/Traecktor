<?php

$included = true;
$front_page = true;
require_once "./core/localization.php";
require_once "./include/header.php";

?>

<body>

	<?php require_once "./include/navbar.php" ?>

	<section class="max-w-screen-lg mx-auto relative mb-4 px-4" style="height: 31rem;">

		<div class="z-0 h-full w-1/2 absolute hidden lg:block"> </div>

		<div class="z-10 lg:flex items-center lg:h-96 relative custom-center text-left">

			<div class="max-w-xl mx-auto ">
				<h1 class="text-primary text-3xl md:text-4xl font-light lg:line-break leading-normal md:leading-relaxed">
					<?php echo t("home.title"); ?>
				</h1>

				<p class="text-1xl mt-3 font-light max-w-xl mx-auto"> <?php echo t("home.slogan"); ?> </p>

				<div class="mt-14 mb-4">
					<a href="#generator" class="btn-primary"><?php echo t("home.generate-complaint"); ?></a>
					<div class="block h-10 sm:hidden"></div>
					<a href="/help.php" class="btn-secondary"><?php echo t("home.view-guide"); ?></a>
				</div>

			</div>

			<div class="h-full w-full hidden lg:block">
				<img src="assets/images/ricky_the_greedy_racoon.png" alt="Ein diebischer Waschbär, der mit einem Sack auf dem Rücken flüchtet. Aufschrift auf dem Sack ist 'Your data'. Der Waschbär denkt sich dabei '€'. Alles umrandet von einem roten Verbotsschild.">
			</div>

		</div>

	</section>

<?php require "./include/counters.php"; ?>

	<div class="md:h-16"></div>

	<section class="max-w-screen-lg mx-auto mb-12 md:mb-24 px-4">

<?php foreach (t("home.simple-faq", []) as $key => $value): ?>
		<h2 class="text-primary text-2xl mb-3"><?php echo $value["question"]; ?></h2>
		<p class="text-primary font-light"><?php echo $value["answer"];?></p>
		<div class="h-8"></div>
<?php endforeach; ?>

	</section>

	<section class="max-w-screen-lg mx-auto px-4">

		<h1 class="text-primary text-3xl md:text-4xl font-medium mb-8 md:mb-16"><?php echo t("home.generate-complaint"); ?></h1>
		<div id="generator"></div>

	</section>

	<div style="display: none" id="portal"></div>

	<?php require_once "./include/footer.php" ?>

</body>
</html>
