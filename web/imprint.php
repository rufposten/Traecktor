<?php
require_once "./core/localization.php";

$included = true;
$html_title = t("imprint.imprint-title");

require "./include/header.php";

?>

<body>

    <?php require_once "./include/navbar.php" ?>

    <div class="max-w-screen-lg mx-auto px-6 lg:px-0">

        <h1 class="text-primary text-3xl md:text-4xl font-medium">
            <?php echo t("imprint.imprint-title"); ?>
        </h1>
        <br />
        <h2 class="text-primary text-2xl mb-3">
            <?php echo t("imprint.imprint-subtitle"); ?>
        </h2>
        <p class="my-3 text-lg">
            <img src="https://rufposten.de/blog/wp-content/uploads/2016/02/rufposten_angaben_kontakt.png" />
        </p>
        <h2 class="text-primary text-2xl mb-3">
            <?php echo t("imprint.privacy-subtitle"); ?>
        </h2>
        <p class="my-3 text-lg">
            <?php echo t("imprint.privacy-text"); ?>
        </p>
        <br />
        <p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/">
            <span property="dct:title">
                Ricky the greedy racoon
            </span>
            by 
            <a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="https://anzui.dev">
                Ondrej Brinkel
            </a> 
            is licensed under
            <a href="http://creativecommons.org/licenses/by-nc/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline;">CC BY-NC 4.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom; display:inline;" src="assets/icons/cc.svg"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom; display:inline;" src="assets/icons/by.svg"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom; display:inline;" src="assets/icons/nc.svg"></a>
        </p>

    </div>

    <?php require_once "./include/footer.php" ?>

</body>

</html>