let start;
function countUp (timestamp) {

    if (start === undefined) start = timestamp;

    const elapsed = timestamp - start;

    countMeUp.forEach((el, index)=>{

        const to = parseInt(el.dataset.to);

        // const newNumber = Math.floor(elapsed / 10);
        const newNumber = Math.floor((elapsed / 30) / 100 * to);

        el.innerHTML = newNumber;

        if (newNumber >= to) {
            el.innerHTML = to;
            countMeUp.splice(index, 1);
        } else {
            el.innerHTML = newNumber;
        }
    })

    if (countMeUp.length != 0 && elapsed < 10000) {
        window.requestAnimationFrame(countUp);
    }

}

window.onload = () => {
    window.requestAnimationFrame(countUp);
}